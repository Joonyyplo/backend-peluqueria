const Product = require("../model/product");

const productController = {};

productController.addProduct = async (req, res) => {
  const newProduct = new Product(
    {
      name: req.body.name,
      price: req.body.price,
      category: req.body.category
    },
    {
      timestamps: true
    }
  );
  await newProduct.save();
  res.json(newProduct)
};

module.exports = productController;