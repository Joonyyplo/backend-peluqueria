const mongoose = require('mongoose');
const url = process.env.MONGODB_URL;
console.log(url);

mongoose.connect(url, {
    useNewUrlParser: true,
    useCreateIndex: true
})

const connection = mongoose.connection;

connection.once('open', () => {
    console.log('DB connected successfully')
})