const { Router } = require('express');

const { addProduct } = require('../controllers/products.controllers')

const router = Router();

router.route('/').post(addProduct)

module.exports = router;