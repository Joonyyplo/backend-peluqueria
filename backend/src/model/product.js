const { Schema, model } = require("mongoose");

const productSchema = new Schema(
  {
    name: String,
    price: Number,
    category: String
  },
  {
    timestamps: true
  }
);

module.exports = model("Product", productSchema);
