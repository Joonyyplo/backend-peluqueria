import React, { useState, useRef } from "react";
import { withRouter } from "react-router-dom";
import Swal from "sweetalert2";

const EditarProducto = props => {
  const nameRef = useRef("");
  const priceRef = useRef("");
  const [stateCategory, setStateCategory] = useState("");

  const { name, price, category, id } = props.productFound;

  const handleChangeCategory = e => {
    setStateCategory(e.target.value);
  };

  const handleSubmit = async e => {
    e.preventDefault();

    const _name = nameRef.current.value;
    const _price = priceRef.current.value;
    const _category = stateCategory === "" ? category : stateCategory;

    if (_name === "" || _price === "" || _category == "") {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Please complete all fields"
      });
      return;
    }

    try {
      const modifiedProduct = {
        name: _name,
        price: _price,
        category: _category
      };

      const request = await fetch(`http://localhost:3001/products/${id}`, {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(modifiedProduct)
      });
      if (request.status === 200) {
        Swal.fire("Good job!", "Your product has been send", "success");
        props.setChangeApi(true);
        props.history.push("/productos");
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <div className="col-md-8 mx-auto ">
        <h1 className="text-center">Agregar nuevo producto o servicio</h1>

        <form className="mt-5" onSubmit={handleSubmit}>
          <div className="form-group">
            <label>Nombre del producto o servicio</label>
            <input
              type="text"
              className="form-control"
              name="nombre"
              placeholder="Nombre del producto o servicio"
              ref={nameRef}
              defaultValue={name}
            />
          </div>

          <div className="form-group">
            <label>Precio del producto o servicio</label>
            <input
              type="number"
              className="form-control"
              name="precio"
              placeholder="Precio del producto o servicio"
              ref={priceRef}
              defaultValue={price}
            />
          </div>

          <h3 className="text-center">Categoría:</h3>
          <div className="text-center">
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                name="categoria"
                value="corte"
                onChange={handleChangeCategory}
                defaultChecked={category === "corte"}
              />
              <label className="form-check-label">Corte</label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                name="categoria"
                value="tintura"
                onChange={handleChangeCategory}
                defaultChecked={category === "tintura"}
              />
              <label className="form-check-label">Tintura</label>
            </div>

            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                name="categoria"
                value="producto"
                onChange={handleChangeCategory}
                defaultChecked={category === "producto"}
              />
              <label className="form-check-label">Producto</label>
            </div>
          </div>

          <button
            type="submit"
            className="font-weight-bold text-uppercase mt-5 btn btn-primary btn-block py-3"
          >
            Agregar Producto
          </button>
        </form>
      </div>
    </div>
  );
};

export default withRouter(EditarProducto);
