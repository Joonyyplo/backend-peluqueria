import React from "react";

const Principal = () => {
  return (
    <div>
      <div class="jumbotron jumbotron-fluid">
        <div class="container text-center">
          <h1 class="display-4 ">Peluqueria & estetica</h1>
          <p class="lead">
            Bienvenidos al sitio diseñado para administrar los servicios de la
            peluqueria React
          </p>
        </div>
      </div>
    </div>
  );
};

export default Principal;
