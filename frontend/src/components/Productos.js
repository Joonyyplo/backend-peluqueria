import React from "react";
import Producto from "./Producto";

const Productos = props => {
  return (
    <div>
      <h1 className="text-center">Productos</h1>
      <ul className="list-group mt-5">
        {props.products.map((prod, id) => (
          <Producto prod={prod} key={id} setChangeApi={props.setChangeApi} />
        ))}
      </ul>
    </div>
  );
};

export default Productos;
