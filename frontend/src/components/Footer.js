import React from "react";

const Footer = () => {
  return (
    <div>
      <p className="text-center mt-4 p2">
        La Peluqueria de react &copy; Todos los derechos reservados
      </p>
    </div>
  );
};

export default Footer;
