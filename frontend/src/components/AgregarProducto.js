import React, { useState } from "react";
import Swal from "sweetalert2";
import { withRouter } from "react-router-dom";

const AgregarProducto = props => {
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [category, setCategory] = useState("");

  const handleChangeCategory = e => {
    setCategory(e.target.value);
  };

  const handleSubmit = async e => {
    e.preventDefault();

    if (name === "" || price === "" || category === "") {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Please complete all fields"
      });
      return;
    }

    try {
      const product = {
        name: name,
        price: price,
        category: category
      };

      const request = await fetch("http://localhost:3001/products", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(product)
      });
      console.log(request);
      if (request.status === 201) {
        Swal.fire("Good job!", "Your product has been send", "success");
        props.setChangeApi(true);
        props.history.push("/productos");
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <div className="col-md-8 mx-auto ">
        <h1 className="text-center">Agregar nuevo producto o servicio</h1>

        <form className="mt-5" onSubmit={handleSubmit}>
          <div className="form-group">
            <label>Nombre del producto o servicio</label>
            <input
              type="text"
              className="form-control"
              name="nombre"
              placeholder="Nombre del producto o servicio"
              onChange={e => setName(e.target.value)}
            />
          </div>

          <div className="form-group">
            <label>Precio del producto o servicio</label>
            <input
              type="number"
              className="form-control"
              name="precio"
              placeholder="Precio del producto o servicio"
              onChange={e => setPrice(parseInt(e.target.value))}
            />
          </div>

          <h3 className="text-center">Categoría:</h3>
          <div className="text-center">
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                name="categoria"
                value="corte"
                onChange={handleChangeCategory}
              />
              <label className="form-check-label">Corte</label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                name="categoria"
                value="tintura"
                onChange={handleChangeCategory}
              />
              <label className="form-check-label">Tintura</label>
            </div>

            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                name="categoria"
                value="producto"
                onChange={handleChangeCategory}
              />
              <label className="form-check-label">Producto</label>
            </div>
          </div>

          <button
            type="submit"
            className="font-weight-bold text-uppercase mt-5 btn btn-primary btn-block py-3"
          >
            Agregar Producto
          </button>
        </form>
      </div>
    </div>
  );
};

export default withRouter(AgregarProducto);
