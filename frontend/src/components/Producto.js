import React from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";

const Producto = props => {
  const { name, price, id } = props.prod;

  const handleDelete = async idProd => {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-primary mx-2",
        cancelButton: "btn btn-danger mx-2"
      },
      buttonsStyling: false
    });

    swalWithBootstrapButtons
      .fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        reverseButtons: true
      })
      .then(async result => {
        if (result.value) {
          try {
            const request = await fetch(
              `http://localhost:3001/products/${idProd}`,
              {
                method: "DELETE",
                headers: { "Content-Type": "application/json" }
              }
            );
            if (request.status === 200) {
              swalWithBootstrapButtons.fire(
                "Deleted!",
                "Your file has been deleted.",
                "success"
              );
            }
            props.setChangeApi(true);
          } catch (error) {
            console.log(error);
          }
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            "Cancelled",
            "Your imaginary file is safe :)",
            "error"
          );
        }
      });
  };

  return (
    <li className="list-group-item d-flex justify-content-between align-items-center">
      <div className="lead">
        {name} <span className="font-weight-bold px-2">${price}</span>
      </div>
      <div>
        <Link to={`/productos/editar/${id}`} className="btn btn-success mr-2">
          Editar
        </Link>
        <button
          className="btn btn-danger"
          type="button"
          onClick={() => handleDelete(id)}
        >
          Eliminar &times;
        </button>
      </div>
    </li>
  );
};

export default Producto;
