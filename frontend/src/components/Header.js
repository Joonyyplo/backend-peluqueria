import React from "react";
import { Link, NavLink } from "react-router-dom";

const Header = () => {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container">
          <Link exact={true} to="/" className="navbar-brand">
            Peluqueria
          </Link>
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <NavLink
                exact={true}
                to="/productos"
                className="nav-link"
                activeClassName="active"
              >
                Productos
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact={true}
                to="/productos/nuevo"
                className="nav-link"
                activeClassName="active"
              >
                Nuevo Producto
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
};

export default Header;
