import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from "./components/Header";
import AgregarProducto from "./components/AgregarProducto";
import Footer from "./components/Footer";
import Principal from "./components/Principal";
import Productos from "./components/Productos";
import EditarProducto from "./components/EditarProducto";
import Producto from "./components/Producto";

function App() {
  const [products, setProducts] = useState([]);
  const [changeAPI, setChangeApi] = useState(true);

  useEffect(() => {
    if (changeAPI === true) {
      handleConsultAPI();
      setChangeApi(false);
    }
  }, [changeAPI]);

  const handleConsultAPI = async () => {
    try {
      const request = await fetch("http://localhost:3001/products");
      const response = await request.json();
      setProducts(response);
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Router>
      <Header />
      <main className="mt-5 container">
        <Switch>
          <Route exact path="/" component={Principal}></Route>
          <Route
            exact
            path="/productos/nuevo"
            render={() => <AgregarProducto setChangeApi={setChangeApi} />}
          ></Route>
          <Route
            exact
            path="/productos/editar/:id"
            render={props => {
              const identifierProduct = parseInt(props.match.params.id);
              const productFound = products.filter(
                prod => prod.id === identifierProduct
              );
              return (
                <EditarProducto
                  setChangeApi={setChangeApi}
                  productFound={productFound[0]}
                />
              );
            }}
          ></Route>
          <Route
            exact
            path="/productos"
            render={() => (
              <Productos setChangeApi={setChangeApi} products={products} />
            )}
          ></Route>
        </Switch>
      </main>
      <Footer />
    </Router>
  );
}

export default App;
